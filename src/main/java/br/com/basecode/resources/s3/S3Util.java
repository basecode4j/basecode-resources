package br.com.basecode.resources.s3;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.s3.model.S3ObjectSummary;


public class S3Util {

	private static final String PATH_SEPARATOR = "/";
	
	private S3Util() {/**/}
	
	/**
	 * Obtém o nome do objeto sem o path dele.<br />
	 * Ex: /path1/path2/path2/file.txt retorna file.txt
	 * @param key A chave do Objeto
	 * @return O nome final do objeto, excluindo o path.
	 */
	public static String getObjectName(String key) {
		if(StringUtils.isBlank(key)) return null;
		if(!key.contains(PATH_SEPARATOR)) return key;
		return key.substring(key.lastIndexOf(PATH_SEPARATOR) + 1);
	}
	
	public static String getObjectName(S3ObjectSummary obj) {
		return getObjectName(obj.getKey());
	}

}