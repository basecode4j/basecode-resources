package br.com.basecode.resources.s3;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.transfer.Upload;

public class S3StoreResult {
	
	private final String bucketName;
	
	private final String key;

	private final PutObjectResult putResult;
	
	private final Upload upload;
	
	private S3StoreResult(String bucketName, String key, PutObjectResult putResult, Upload upload) {
		super();
		this.bucketName = bucketName;
		this.key = key;
		this.putResult = putResult;
		this.upload = upload;
	}

	public S3StoreResult(String bucketName, String key, PutObjectResult putResult) {
		this(bucketName, key, putResult, null);
	}
	
	public S3StoreResult(String bucketName, String key, Upload upload) {
		this(bucketName, key, null, upload);
	}
	
	public String getBucketName() {
		return bucketName;
	}
	
	public String getKey() {
		return key;
	}
	
	public Upload getUpload() {
		return upload;
	}
	
	public PutObjectResult getPutResult() {
		return putResult;
	}
	
	public boolean isMultipart() {
		return this.upload != null;
	}
	
	public boolean isEmpty() {
		return this.bucketName == null;
	}
	
	public static S3StoreResult empty() {
		return new S3StoreResult(null, null, null, null);
	}
	
	public long getStoredSize() {
		if(isMultipart()) {
			return this.upload.getProgress().getBytesTransferred();
		}
		return this.putResult.getMetadata().getContentLength();
	}
	
}
