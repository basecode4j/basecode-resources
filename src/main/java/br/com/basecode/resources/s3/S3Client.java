package br.com.basecode.resources.s3;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public interface S3Client {
	
	Bucket makeBucket(String name);
	
	void deleteBucket(String name);
	
	void emptyBucket(String name);
	
	CopyObjectResult move(CopyObjectRequest moveRequest);
	
	CopyObjectResult move(String bucket, String sourceKey, String targetKey);

	AmazonS3 s3();
	
	List<S3ObjectSummary> listSummaries(String bucketName, String prefix);
	
	List<S3ObjectSummary> listSummaries(String bucketName);
	
	URL makeUrl(String bucketName, String objectKey);
	
	URL makeUrl(String bucketName, String objectKey, int expirationHours);
	
	S3StoreResult storeStream(String bucketName, String key, InputStream stream) throws IOException, InterruptedException;
	
	S3StoreResult storeStream(String bucketName, String key, InputStream stream, ObjectMetadata meta) throws IOException, InterruptedException;
	
	S3StoreResult storeFile(String bucketName, String key, File file) throws IOException, InterruptedException;
	
	S3StoreResult storeFile(String bucketName, String key, File file, ObjectMetadata metaData) throws IOException, InterruptedException;

	ByteArrayInputStream getObjectAsByteArrayInputStream(String bucketName, String key) throws IOException;
	
	BufferedImage getObjectAsImage(String bucketName, String key) throws IOException;
	
	
}
