package br.com.basecode.resources.s3.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.IOUtils;

import br.com.basecode.resources.s3.S3Client;
import br.com.basecode.resources.s3.S3StoreResult;

public class S3ClientImpl implements S3Client{

	private final AmazonS3 conn;

	private static final int DEFAULT_URL_EXP_HOURS = 8;

	/** Limite em bytes para o upload em uma parte. 5MB atualmente
	 *  A Amazon recomenda que uploads grandes sejam feitos em várias partes.
	 *  É possível fazer upload multipart a partir de 5MB, então esse valor está sendo definido como o padrão. 
	 **/
	private static final long SINGLE_PART_LIMIT = 5242880;

	public S3ClientImpl(AmazonS3 conn) {
		this.conn = conn;
	}
	
	public static S3ClientImpl createInstance(){
		
		Regions region = Regions.fromName(System.getenv("AWS_REGION"));
		
		AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(region).build();
		
		return new S3ClientImpl(s3);
		
	}

	@Override
	public Bucket makeBucket(String name) {
		if(s3().doesBucketExistV2(name)) {
			return s3().listBuckets().stream().filter(b -> b.getName().equals(name)).findFirst().orElse(null);
		}
		return s3().createBucket(name);
	}

	@Override
	public CopyObjectResult move(CopyObjectRequest copyRequest) {

		final ObjectMetadata metaData;

		final CopyObjectResult copyResult;

		metaData = s3().getObjectMetadata(copyRequest.getSourceBucketName(), copyRequest.getSourceKey());

		copyRequest.setNewObjectMetadata(metaData);

		copyResult = s3().copyObject(copyRequest);

		s3().deleteObject(copyRequest.getSourceBucketName(), copyRequest.getSourceKey());

		return copyResult;

	}

	@Override
	public CopyObjectResult move(String bucketName, String sourceKey, String targetKey) {
		
		CopyObjectRequest copyRequest = new CopyObjectRequest();
		
		copyRequest.setSourceBucketName(bucketName);
		
		copyRequest.setDestinationBucketName(bucketName);
		
		copyRequest.setSourceKey(sourceKey);
		
		copyRequest.setDestinationKey(targetKey);
		
		return move(copyRequest);
		
	}
	
	@Override
	public AmazonS3 s3() {
		return this.conn;
	}

	@Override
	public List<S3ObjectSummary> listSummaries(String bucketName, String prefix) {

		ObjectListing listing;

		final List<S3ObjectSummary> summaries;

		final ListObjectsRequest request;

		summaries = new ArrayList<>();

		request = new ListObjectsRequest();

		request.setBucketName(bucketName);

		request.setPrefix(prefix);

		listing = s3().listObjects(request);
		
		summaries.addAll(listing.getObjectSummaries());

		while(listing.isTruncated()) {
			listing = s3().listNextBatchOfObjects(listing);
			summaries.addAll(listing.getObjectSummaries());
		}

		return summaries;

	}

	@Override
	public List<S3ObjectSummary> listSummaries(String bucketName) {
		return listSummaries(bucketName, null);
	}

	@Override
	public URL makeUrl(String bucketName, String objectKey) {
		return makeUrl(bucketName, objectKey, DEFAULT_URL_EXP_HOURS);
	}

	@Override
	public URL makeUrl(String bucketName, String objectKey, int expirationHours) {
		Date expirationDate = (new DateTime()).plusHours(expirationHours).toDate();
		GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(bucketName, objectKey);
		urlRequest.withExpiration(expirationDate);
		return s3().generatePresignedUrl(urlRequest);
	}

	protected boolean isMultipart(File file) {
		return FileUtils.sizeOf(file) > SINGLE_PART_LIMIT;
	}

	protected boolean isMultipart(InputStream is) throws IOException {
		return is.available() > SINGLE_PART_LIMIT;
	}

	public S3StoreResult storeStream(String bucketName, String key, InputStream is) throws IOException, InterruptedException{
		return storeStream(bucketName, key, is, null);
	}

	@Override
	public S3StoreResult storeStream(String bucketName, String key, InputStream is, ObjectMetadata meta) throws IOException, InterruptedException{
		makeBucket(bucketName);
		final ObjectMetadata metaData;
		metaData = ObjectUtils.defaultIfNull(meta, new ObjectMetadata());
		metaData.setContentLength(is.available());
		final PutObjectRequest request = new PutObjectRequest(bucketName, key, is, metaData);		
		if(isMultipart(is)) {
			TransferManager tm = TransferManagerBuilder.standard().withS3Client(s3()).build();
			Upload upload = tm.upload(request);
			upload.waitForCompletion();
			return new S3StoreResult(bucketName, key, upload);
		}else {
			PutObjectResult putObject = s3().putObject(request);
			putObject.getMetadata().setContentLength(s3().getObjectMetadata(bucketName, key).getContentLength());
			return new S3StoreResult(bucketName, key, putObject);
		}
	}

	@Override
	public S3StoreResult storeFile(String bucketName, String key, File file) throws IOException, InterruptedException {
		return storeFile(bucketName, key, file, null);
	}

	@Override
	public S3StoreResult storeFile(String bucketName, String key, File file, ObjectMetadata meta) throws IOException, InterruptedException {
		makeBucket(bucketName);
		final PutObjectRequest request = new PutObjectRequest(bucketName, key, file);
		request.setMetadata(meta);
		if(isMultipart(file)) {
			TransferManager tm = TransferManagerBuilder.standard().withS3Client(s3()).build();
			Upload upload = tm.upload(request);
			upload.waitForCompletion();
			return new S3StoreResult(bucketName, key, upload);
		}else {
			PutObjectResult putObject = s3().putObject(request);
			putObject.getMetadata().setContentLength(s3().getObjectMetadata(bucketName, key).getContentLength());
			return new S3StoreResult(bucketName, key, putObject);
		}
	}

	@Override
	public ByteArrayInputStream getObjectAsByteArrayInputStream(String bucketName, String key) throws IOException {
		return new ByteArrayInputStream(IOUtils.toByteArray(s3().getObject(new GetObjectRequest(bucketName, key)).getObjectContent()));
	}
	
	@Override
	public BufferedImage getObjectAsImage(String bucketName, String key) throws IOException{
		return ImageIO.read(getObjectAsByteArrayInputStream(bucketName, key));
	}

	@Override
	public void deleteBucket(String name) {
		emptyBucket(name);
		s3().deleteBucket(name);
	}

	@Override
	public void emptyBucket(String name) {
		listSummaries(name).forEach(summary -> {
			s3().deleteObject(summary.getBucketName(), summary.getKey());
		});
	}

}
