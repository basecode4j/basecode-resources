package br.com.basecode.resources.file;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public enum Extension {

	XML("xml"),TIF("tif"),JPEG("jpg"),PNG("png"),ZIP("zip"),LOG("log"),TXT("txt"),SD1("sd1"),TAR("tar"),SGD("sgd"),NENHUMA(null);
	
	private String nome;
	
	private static final Map<String, Extension> CODE_MAP = new HashMap<>();
	
	static {
		for(Extension e: Extension.values()) {
			if(NENHUMA.equals(e)) continue;
			CODE_MAP.put(e.getNome().toLowerCase(), e);
		}
	}
	
	private Extension(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getUpper() {
		return this.nome.toUpperCase();
	}
	
	public String[] getNomes() {
		return new String[]{this.nome.toLowerCase(), this.nome.toUpperCase()};
	}
	
	/**
	 * Retorna o nome da extensão com o (.) ponto. 
	 * Ex: para zip, retorna .zip
	 * @return A extensão.
	 */
	public String getFull() {
		
		if(hasNoName()) {
			return StringUtils.EMPTY;
		}
		
		return ".".concat(getNome());
	}
	
	public boolean hasNoName() {
		return NENHUMA.equals(this);
	}
	
	public static Extension getByNome(String nome) {
		if(StringUtils.isBlank(nome)) return null;
		String shortName = nome.replace(".", "");
		return CODE_MAP.get(shortName.toLowerCase());
	}
	
	public static List<Extension> usedValues() {
		return CODE_MAP.values().stream().collect(Collectors.toList());
	}
	
	
}